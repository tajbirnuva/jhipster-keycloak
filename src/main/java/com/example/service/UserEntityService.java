package com.example.service;

import com.example.domain.UserEntity;
import com.example.repository.UserEntityRepository;
import com.example.service.dto.UserEntityDTO;
import com.example.service.mapper.UserEntityMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.example.domain.UserEntity}.
 */
@Service
@Transactional
public class UserEntityService {

    private final Logger log = LoggerFactory.getLogger(UserEntityService.class);

    private final UserEntityRepository userEntityRepository;

    private final UserEntityMapper userEntityMapper;

    public UserEntityService(UserEntityRepository userEntityRepository, UserEntityMapper userEntityMapper) {
        this.userEntityRepository = userEntityRepository;
        this.userEntityMapper = userEntityMapper;
    }

    /**
     * Save a userEntity.
     *
     * @param userEntityDTO the entity to save.
     * @return the persisted entity.
     */
    public UserEntityDTO save(UserEntityDTO userEntityDTO) {
        log.debug("Request to save UserEntity : {}", userEntityDTO);
        UserEntity userEntity = userEntityMapper.toEntity(userEntityDTO);
        userEntity = userEntityRepository.save(userEntity);
        return userEntityMapper.toDto(userEntity);
    }

    /**
     * Update a userEntity.
     *
     * @param userEntityDTO the entity to save.
     * @return the persisted entity.
     */
    public UserEntityDTO update(UserEntityDTO userEntityDTO) {
        log.debug("Request to update UserEntity : {}", userEntityDTO);
        UserEntity userEntity = userEntityMapper.toEntity(userEntityDTO);
        userEntity = userEntityRepository.save(userEntity);
        return userEntityMapper.toDto(userEntity);
    }

    /**
     * Partially update a userEntity.
     *
     * @param userEntityDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<UserEntityDTO> partialUpdate(UserEntityDTO userEntityDTO) {
        log.debug("Request to partially update UserEntity : {}", userEntityDTO);

        return userEntityRepository
            .findById(userEntityDTO.getId())
            .map(existingUserEntity -> {
                userEntityMapper.partialUpdate(existingUserEntity, userEntityDTO);

                return existingUserEntity;
            })
            .map(userEntityRepository::save)
            .map(userEntityMapper::toDto);
    }

    /**
     * Get all the userEntities.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UserEntityDTO> findAll() {
        log.debug("Request to get all UserEntities");
        return userEntityRepository.findAll().stream().map(userEntityMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one userEntity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserEntityDTO> findOne(Long id) {
        log.debug("Request to get UserEntity : {}", id);
        return userEntityRepository.findById(id).map(userEntityMapper::toDto);
    }

    /**
     * Delete the userEntity by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserEntity : {}", id);
        userEntityRepository.deleteById(id);
    }
}
