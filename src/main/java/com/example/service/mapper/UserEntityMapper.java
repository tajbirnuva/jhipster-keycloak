package com.example.service.mapper;

import com.example.domain.UserEntity;
import com.example.service.dto.UserEntityDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserEntity} and its DTO {@link UserEntityDTO}.
 */
@Mapper(componentModel = "spring")
public interface UserEntityMapper extends EntityMapper<UserEntityDTO, UserEntity> {}
