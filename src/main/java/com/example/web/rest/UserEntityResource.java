package com.example.web.rest;

import com.example.repository.UserEntityRepository;
import com.example.service.UserEntityService;
import com.example.service.dto.UserEntityDTO;
import com.example.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.example.domain.UserEntity}.
 */
@RestController
@RequestMapping("/api/user-entities")
public class UserEntityResource {

    private final Logger log = LoggerFactory.getLogger(UserEntityResource.class);

    private static final String ENTITY_NAME = "userEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserEntityService userEntityService;

    private final UserEntityRepository userEntityRepository;

    public UserEntityResource(UserEntityService userEntityService, UserEntityRepository userEntityRepository) {
        this.userEntityService = userEntityService;
        this.userEntityRepository = userEntityRepository;
    }

    /**
     * {@code POST  /user-entities} : Create a new userEntity.
     *
     * @param userEntityDTO the userEntityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userEntityDTO, or with status {@code 400 (Bad Request)} if the userEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<UserEntityDTO> createUserEntity(@RequestBody UserEntityDTO userEntityDTO) throws URISyntaxException {
        log.debug("REST request to save UserEntity : {}", userEntityDTO);
        if (userEntityDTO.getId() != null) {
            throw new BadRequestAlertException("A new userEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserEntityDTO result = userEntityService.save(userEntityDTO);
        return ResponseEntity
            .created(new URI("/api/user-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-entities/:id} : Updates an existing userEntity.
     *
     * @param id the id of the userEntityDTO to save.
     * @param userEntityDTO the userEntityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userEntityDTO,
     * or with status {@code 400 (Bad Request)} if the userEntityDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userEntityDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<UserEntityDTO> updateUserEntity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserEntityDTO userEntityDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserEntity : {}, {}", id, userEntityDTO);
        if (userEntityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userEntityDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userEntityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserEntityDTO result = userEntityService.update(userEntityDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userEntityDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-entities/:id} : Partial updates given fields of an existing userEntity, field will ignore if it is null
     *
     * @param id the id of the userEntityDTO to save.
     * @param userEntityDTO the userEntityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userEntityDTO,
     * or with status {@code 400 (Bad Request)} if the userEntityDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userEntityDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userEntityDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserEntityDTO> partialUpdateUserEntity(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody UserEntityDTO userEntityDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserEntity partially : {}, {}", id, userEntityDTO);
        if (userEntityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userEntityDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userEntityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserEntityDTO> result = userEntityService.partialUpdate(userEntityDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userEntityDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-entities} : get all the userEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userEntities in body.
     */
    @GetMapping("")
    public List<UserEntityDTO> getAllUserEntities() {
        log.debug("REST request to get all UserEntities");
        return userEntityService.findAll();
    }

    /**
     * {@code GET  /user-entities/:id} : get the "id" userEntity.
     *
     * @param id the id of the userEntityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userEntityDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserEntityDTO> getUserEntity(@PathVariable Long id) {
        log.debug("REST request to get UserEntity : {}", id);
        Optional<UserEntityDTO> userEntityDTO = userEntityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userEntityDTO);
    }

    /**
     * {@code DELETE  /user-entities/:id} : delete the "id" userEntity.
     *
     * @param id the id of the userEntityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUserEntity(@PathVariable Long id) {
        log.debug("REST request to delete UserEntity : {}", id);
        userEntityService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
