export interface IEmployee {
  id: number;
  employeeName?: string | null;
  address?: string | null;
}

export type NewEmployee = Omit<IEmployee, 'id'> & { id: null };
