import { TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness, RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { UserEntityDetailComponent } from './user-entity-detail.component';

describe('UserEntity Management Detail Component', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserEntityDetailComponent, RouterTestingModule.withRoutes([], { bindToComponentInputs: true })],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: UserEntityDetailComponent,
              resolve: { userEntity: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(UserEntityDetailComponent, '')
      .compileComponents();
  });

  describe('OnInit', () => {
    it('Should load userEntity on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', UserEntityDetailComponent);

      // THEN
      expect(instance.userEntity).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
