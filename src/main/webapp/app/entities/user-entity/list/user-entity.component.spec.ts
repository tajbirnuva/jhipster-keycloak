import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { UserEntityService } from '../service/user-entity.service';

import { UserEntityComponent } from './user-entity.component';

describe('UserEntity Management Component', () => {
  let comp: UserEntityComponent;
  let fixture: ComponentFixture<UserEntityComponent>;
  let service: UserEntityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'user-entity', component: UserEntityComponent }]),
        HttpClientTestingModule,
        UserEntityComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              }),
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(UserEntityComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(UserEntityComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(UserEntityService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        }),
      ),
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.userEntities?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to userEntityService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getUserEntityIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getUserEntityIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
