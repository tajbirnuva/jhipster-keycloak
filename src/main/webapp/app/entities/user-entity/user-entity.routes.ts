import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { UserEntityComponent } from './list/user-entity.component';
import { UserEntityDetailComponent } from './detail/user-entity-detail.component';
import { UserEntityUpdateComponent } from './update/user-entity-update.component';
import UserEntityResolve from './route/user-entity-routing-resolve.service';

const userEntityRoute: Routes = [
  {
    path: '',
    component: UserEntityComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UserEntityDetailComponent,
    resolve: {
      userEntity: UserEntityResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UserEntityUpdateComponent,
    resolve: {
      userEntity: UserEntityResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UserEntityUpdateComponent,
    resolve: {
      userEntity: UserEntityResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default userEntityRoute;
