import { IUserEntity, NewUserEntity } from './user-entity.model';

export const sampleWithRequiredData: IUserEntity = {
  id: 9154,
};

export const sampleWithPartialData: IUserEntity = {
  id: 31382,
  firstname: 'without run',
  lastname: 'crust',
  email: 'Elmore_Kessler@yahoo.com',
  password: 'joshingly so',
};

export const sampleWithFullData: IUserEntity = {
  id: 2619,
  firstname: 'energetically opulent',
  lastname: 'noted instead',
  username: 'frenetically evenly lead',
  email: 'Gunner_Anderson@gmail.com',
  password: 'flow',
  roles: 'concerning',
};

export const sampleWithNewData: NewUserEntity = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
