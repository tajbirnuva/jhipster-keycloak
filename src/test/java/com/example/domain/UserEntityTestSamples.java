package com.example.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class UserEntityTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static UserEntity getUserEntitySample1() {
        return new UserEntity()
            .id(1L)
            .firstname("firstname1")
            .lastname("lastname1")
            .username("username1")
            .email("email1")
            .password("password1")
            .roles("roles1");
    }

    public static UserEntity getUserEntitySample2() {
        return new UserEntity()
            .id(2L)
            .firstname("firstname2")
            .lastname("lastname2")
            .username("username2")
            .email("email2")
            .password("password2")
            .roles("roles2");
    }

    public static UserEntity getUserEntityRandomSampleGenerator() {
        return new UserEntity()
            .id(longCount.incrementAndGet())
            .firstname(UUID.randomUUID().toString())
            .lastname(UUID.randomUUID().toString())
            .username(UUID.randomUUID().toString())
            .email(UUID.randomUUID().toString())
            .password(UUID.randomUUID().toString())
            .roles(UUID.randomUUID().toString());
    }
}
